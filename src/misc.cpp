#include "../inc/misc.hpp"
#include <Arduino.h>

const uint8_t PUMP_CTL_PIN = 16;
const uint8_t TEMP_CTL_PIN = 15;
const uint8_t TEMP_IN_PIN = 0;

const double TEMP_OFFSET_MV = 750;
const double TEMP_GRADIENT_MV = 10;
const double TEMP_OFFSET_C = 25;

// initialise pump ssr, temp ssr
void MISC_Init(void)
{
  digitalWrite(PUMP_CTL_PIN, 0);
  digitalWrite(TEMP_CTL_PIN, 0);
  pinMode(PUMP_CTL_PIN, OUTPUT);
  pinMode(TEMP_CTL_PIN, OUTPUT);
  analogReadResolution(12);
}

void MISC_PumpOn(void)
{
  digitalWrite(PUMP_CTL_PIN, 1);
}

void MISC_PumpOff(void)
{
  digitalWrite(PUMP_CTL_PIN, 0);
}

void MISC_TempSSROn(void)
{
  digitalWrite(TEMP_CTL_PIN, 1);
}

void MISC_TempSSROff(void)
{
  digitalWrite(TEMP_CTL_PIN, 0);
}

double MISC_ReadTemp(void)
{
  uint16_t result = analogRead(TEMP_IN_PIN);
  double temp = double(result) / 4095.0;
  temp *= 3300;
  temp -= TEMP_OFFSET_MV;
  temp /= TEMP_GRADIENT_MV;
  return TEMP_OFFSET_C + temp;
}
