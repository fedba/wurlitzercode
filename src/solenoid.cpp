#include "../inc/solenoid.hpp"
#include "../inc/expander.hpp"

// pin mapping
// PE1
// sol 0-14
const uint8_t PE1_MAPPING[] = {7,6,5,4,3,2,1,0,15,14,13,12,11,10,9};
// PE2
// sol 15-24 (0 - 9), sol 29-34 (10-15)
const uint8_t PE2_MAPPING[] = {7,6,5,4,3,2,1,0,15,14,13,12,11,10,9,8};
// PE3
// sol 25-28
const uint8_t PE3_MAPPING[] = {15,14,13,12};


void SOL_Init(void)
{
  EXPANDER_Init();

  // All outputs low
  EXPANDER_PortWrite(EXPANDER1, 0); // all outputs low (off)
  EXPANDER_PortWrite(EXPANDER2, 0); // all outputs low (off)
  EXPANDER_PortWrite(EXPANDER3, 0); // all outputs low (off)


  // set pin directions
  EXPANDER_PortDirection(EXPANDER1, 0x0);
  EXPANDER_PortDirection(EXPANDER2, 0x0);
  EXPANDER_PortDirection(EXPANDER3, 0x0FFF);
}

void SOL_SetSingle(uint8_t solNum, bool value)
{
  if (solNum >= NUM_SOLENOIDS) return;
  if (solNum < 15) {
    // PE 1
    EXPANDER_PinWrite(EXPANDER1, PE1_MAPPING[solNum], value);
  } else if (solNum < 31 ) {
    // PE 2
    EXPANDER_PinWrite(EXPANDER2, PE2_MAPPING[solNum - 15], value);
  } else {
    // PE 3
    EXPANDER_PinWrite(EXPANDER3, PE3_MAPPING[solNum - 31], value);
  }
}

void SOL_SetAll(uint64_t data)
{
  uint16_t expanderData[3] = {0};

  for (uint16_t i = 0; i < 16; i++) {
    if (i < 15 && (data & (1 << i))) expanderData[0] |= (1 << PE1_MAPPING[i]);
    if (i < 10 && (data & (1 << (i + 15)))) expanderData[1] |= (1 << PE2_MAPPING[i]);
    if (i >= 10 && (data & (1 << (i + 25 - 10)))) expanderData[1] |= (1 << PE2_MAPPING[i]);
    if (i < 4 && ((data >> 31) & (1 << i))) expanderData[2] |= (1 << PE3_MAPPING[i]);
  }

  EXPANDER_PortWrite(EXPANDER1, expanderData[0]);
  EXPANDER_PortWrite(EXPANDER2, expanderData[1]);
  EXPANDER_PortWrite(EXPANDER3, expanderData[2]);
}
