//----------------------------------------------------------------------------------------
/*!
 \file       expander.cpp
 \modified
 \copyright
 \license
 \brief      Implementation of i2c expander driver
 \author     $Author$
 \date       $Date$
 */
//----------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
// Headers
//---------------------------------------------------------------------------------------

#include "../inc/expander.hpp"
#include <Wire.h>

const uint8_t NXP9555_INPUT_REG = 0x0;
const uint8_t NXP9555_OUTPUT_REG = 0x02;
const uint8_t NXP9555_INVERT_REG = 0x04;
const uint8_t NXP9555_CONFIG_REG = 0x06;

uint16_t outputCache[3] = {0};

// used by port write and pin write
void portWriteNoCache(uint8_t expander, uint16_t data);

bool EXPANDER_Init(void)
{
  Wire.begin();
  EXPANDER_PortWrite(EXPANDER1, outputCache[0]);
  EXPANDER_PortWrite(EXPANDER2, outputCache[1]);
  EXPANDER_PortWrite(EXPANDER3, outputCache[2]);
  return true;
}

//----------------------------------------------------------------------------------------
void EXPANDER_PortDirection(uint8_t expander, uint16_t dir)
{
	// nb - high = input, low = output
	Wire.beginTransmission(expander);
	Wire.write(NXP9555_CONFIG_REG);
	Wire.write(0xff & dir);  // low byte
	Wire.write(dir >> 8);    // high byte
	Wire.endTransmission();
}

// used by port write and pin write
void portWriteNoCache(uint8_t expander, uint16_t data)
{
  Wire.beginTransmission(expander);
  Wire.write(NXP9555_OUTPUT_REG);
  Wire.write(0xff & data);  //  low byte
  Wire.write(data >> 8);    //  high byte
  Wire.endTransmission();
}

void EXPANDER_PortWrite(uint8_t expander, uint16_t data)
{
  portWriteNoCache(expander, data);
  if (expander == EXPANDER1) outputCache[0] = data;
  else if (expander == EXPANDER2) outputCache[1] = data;
  else outputCache[2] = data;
}

void EXPANDER_PinWrite(uint8_t expander, uint16_t pin, bool value)
{
  if (expander == EXPANDER1) {
    if (value) outputCache[0] |= (1 << pin);
    else outputCache[0] &= ~(1 << pin);
    portWriteNoCache(EXPANDER1, outputCache[0]);
  } else if (expander == EXPANDER2) {
    if (value) outputCache[1] |= (1 << pin);
    else outputCache[1] &= ~(1 << pin);
    portWriteNoCache(EXPANDER2, outputCache[1]);
  } else {
    if (value) outputCache[2] |= (1 << pin);
    else outputCache[2] &= ~(1 << pin);
    portWriteNoCache(EXPANDER3, outputCache[2]);
  }
}

uint16_t EXPANDER_PortRead(uint8_t expander, bool *error)
{
	uint16_t _data = 0;
	*error = false;
	Wire.beginTransmission(expander);
	Wire.write(NXP9555_INPUT_REG);
	Wire.endTransmission();
	Wire.requestFrom(expander, (uint8_t)2);
	if (Wire.available())
		_data = Wire.read();
	else
		*error = true;

	if (Wire.available())
		_data |= (Wire.read() << 8);
	else
		*error = true;

	return _data;
}


// bool EXPANDER_Init(void)
// {
// 	if (CORE.expandersActive == false)
// 	{
// 		uint16_t dummy;
// 		bool fault;
//
// 		POWER_V3V3Enable(ON); // ensure power is supplied
// 		pinMode(NXP9555_INT_PIN, INPUT_PULLUP);  // to read /INT
// 		MISC_StartTWI();
// 		delay(20); // need this to ensure power stable
//
// 		// do a dummy read to see if device is present
// 		//x = EXPANDER_PortRead(&fault);
// 		dummy = EXPANDER_PortRead(EXPANDER_U1, &fault);
// 		if (fault)
// 		{
// 			Serial.println("TWI Expander 1 Init FAIL!!!");
// 			CORE.expandersActive = false;
// 			return false;
// 		}
// 		dummy = EXPANDER_PortRead(EXPANDER_U2, &fault);
// 		if (fault)
// 		{
// 			Serial.println("TWI Expander 2 Init FAIL!!!");
// 			CORE.expandersActive = false;
// 			return false;
// 		}
//
// 		// set the inputs & outputs
// 		Serial.println("Setting Expander I/0 ");
// 		//EXPANDER_PortWrite(0); // all outputs low (off)
// 		EXPANDER_PortWrite(EXPANDER_U1, 0); // all outputs low (off)
// 		EXPANDER_PortWrite(EXPANDER_U2, 0); // all outputs low (off)
// 		//EXPANDER_PortDirection(EXPANDER_PIN_SETTING);
// 		EXPANDER_PortDirection(EXPANDER_U1, EXPANDER_U1_PIN_DIRECTIONS);
// 		EXPANDER_PortDirection(EXPANDER_U2, EXPANDER_U2_PIN_DIRECTIONS);
// 		CORE.expandersActive = true;
// 		//CORE.expanderinput = ????
// 	}
//
// 	return true; // success
// }



//----------------------------------------------------------------------------------------


// //----------------------------------------------------------------------------------------
// inline bool EXPANDER_InterruptRead()
// {
// 	return digitalRead(NXP9555_INT_PIN);
// }


//----------------------------------------------------------------------------------------
// Static functions
//----------------------------------------------------------------------------------------
