#include "../inc/switch.hpp"
#include "../inc/expander.hpp"

const uint8_t PIN_MAPPING[] = {0,1,2,3,4,5,6,7,11,10,9,8};

void SWITCH_Init(void)
{
  // pins 0.0 - 1.3 (lower 3 nibbles)
  EXPANDER_PortDirection(EXPANDER3, 0x0FFF);
}

uint16_t SWITCH_ReadAll(void)
{
  bool error = false;
  uint16_t temp = EXPANDER_PortRead(EXPANDER3, &error);
  uint16_t result = (~temp) & 0xFF; // invert because active low
  for (uint8_t i = 8; i < 12; i++) {
    if (!(temp & (1 << PIN_MAPPING[i]))) result |= (1 << i); // ! because active low
  }
  return result;
}

bool SWITCH_ReadSingle(uint8_t switchNum)
{
  if (switchNum >= 12) return false;
  return (SWITCH_ReadAll() & (1 << switchNum));
}
