# Solenoid Gas Loop Drivers

Requires the [teensy addon for arduino](https://www.pjrc.com/teensy/td_download.html)

To download `git clone https://bitbucket.org/natfaulk2/solenoidgasloopdrivers.git solenoidGasLoop`
Otherwise rename downloaded folder from solenoidGasLoopDrivers to solenoidGasLoop else arduino will complain

## Functions

### Expander init
Initialises the I2C port expanders. Call before doing anything else.

`EXPANDER_Init();`

### Initialise solonoids
Initialise solonoids

`void SOL_Init(void);`

### Set single solonoid
Sets the value of a single solonoid. Pass in solonoid number and value (true or false for on or off). If changing multiple solonoids at once prefer `SOL_SetAll()`. This is because each call to `SOL_SetSingle()` writes to the i2c port expander which is fairly slow (compared to setting a normal GPIO).

`void SOL_SetSingle(uint8_t solNum, bool value);`

### Set all solonoids
Set the value of all the solonoids with each represented by 1 bit for the first 35 bits of a uint64

`void SOL_SetAll(uint64_t data);`

### initialise switches
Initialise solonoids

`void SWITCH_Init(void);`

### Read single switch
Reads single switch, returns true or false for pressed or not pressed. For similar reasons to `SOL_SetSingle()` vs `SOL_SetAll()` prefer `SWITCH_ReadAll()` when reading multiple switches at once.

`bool SWITCH_ReadSingle(uint8_t switchNum);`


### Read all switches
Reads all switches. Returns a uint 16. 1 bit per switch for the first 12 bits

`uint16_t SWITCH_ReadAll(void);`

### Initialise misc peripherals
Initialise misc peripherals

`void MISC_Init(void);`

### Pump digital output control
Turn pump on or off

```
void MISC_PumpOn(void);
void MISC_PumpOff(void);
```

### Temperature SSR digital output control
Turn Temperature SSR on or off

```
void MISC_TempSSROn(void);
void MISC_TempSSROff(void);
```

### Read analog temperature sensor
Read analog digital output sensor. 12bit ADC. Returns a double with units of degrees Celsius.

`double MISC_ReadTemp(void);`
