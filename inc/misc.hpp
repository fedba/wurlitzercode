#ifndef MISC_HPP
#define MISC_HPP

void MISC_Init(void);
void MISC_PumpOn(void);
void MISC_PumpOff(void);
void MISC_TempSSROn(void);
void MISC_TempSSROff(void);
double MISC_ReadTemp(void);

#endif /* end of include guard: MISC_HPP */
