#ifndef SWITCH_HPP
#define SWITCH_HPP

#include <stdint.h>

void SWITCH_Init(void);
uint16_t SWITCH_ReadAll(void);
bool SWITCH_ReadSingle(uint8_t switchNum);

#endif /* end of include guard: SWITCH_HPP */
