#ifndef SOLENOID_HPP
#define SOLENOID_HPP

#include <stdint.h>

const uint8_t NUM_SOLENOIDS = 35;

void SOL_Init(void);
void SOL_SetSingle(uint8_t solNum, bool value);
void SOL_SetAll(uint64_t data);


#endif /* end of include guard: SOLENOID_HPP */
