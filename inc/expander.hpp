#ifndef EXPANDER_HPP
#define EXPANDER_HPP

#include <stdint.h>

// 3 expanders
const uint8_t EXPANDER1 = 0x20;
const uint8_t EXPANDER2 = 0x21;
const uint8_t EXPANDER3 = 0x22;

bool EXPANDER_Init(void);
void EXPANDER_PortDirection(uint8_t expander, uint16_t dir);
void EXPANDER_PortWrite(uint8_t expander, uint16_t data);
void EXPANDER_PinWrite(uint8_t expander, uint16_t pin, bool value);
uint16_t EXPANDER_PortRead(uint8_t expander, bool *error);


#endif /* end of include guard: EXPANDER_HPP */
