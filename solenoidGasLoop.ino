/*TO DO Still
need to make delays between activation of multiple commands to allow multiple doses
Make time for introductions more adjustable
make clearing LOG possible in interface Program
make temp conntrol actually proportionalrather than just two stage

*/


#include <Time.h>
#include <TimeLib.h>
#include <EEPROM.h>



#include "inc/expander.hpp"
#include "inc/misc.hpp"
#include "inc/solenoid.hpp"
#include "inc/switch.hpp"
#include <Wire.h>



// call before doing anything else
// EXPANDER_Init();

// initialise solonoids
// void SOL_Init(void);

// set single solonoid. Pass in solonoid number and value (true of false for on or off)
// void SOL_SetSingle(uint8_t solNum, bool value);

// 1 bit per solonoid for the first 35 bits. Requires a uint 64 as 35 solonoids
// void SOL_SetAll(uint64_t data);

// initialise switches
// void SWITCH_Init(void);

// read all switches. returns a uint 16. 1 bit per switch for lowest 12 bits
// uint16_t SWITCH_ReadAll(void);

// read single switch, returns true or false for pressed or not pressed
// bool SWITCH_ReadSingle(uint8_t switchNum);

// initial misc peripherals
// void MISC_Init(void);

// pump digital output on or off
// void MISC_PumpOn(void);
// void MISC_PumpOff(void);

// temp ssr digital output on or off
// void MISC_TempSSROn(void);
// void MISC_TempSSROff(void);

//  read analog temp sensor. returns a double in celsius
// double MISC_ReadTemp(void);

// Note: when doing multiple switch reads or solonoid writes prefer the set / read all.
// setting / reading single requires a register read / write to the I2c expander
// for each which is reasonably slow due to the I2C line.



//setting switch number for mode
#define wurlitzerName "Barry"

#define autoDose 0
#define flushTest 1
#define flushDoses 2
#define flushPlumbing 3
#define PrimeDoses 4
#define flushSystem 5
#define closeAllPumpOff 6
#define flushPrimeChamber 7
#define manual 8
#define cascadeMode 9

#define tempBufferSize 5


//Control solenoid activation
uint64_t autoMode (uint8_t cycle);
uint64_t cascade(uint64_t soldenoidsActive, uint16_t pulseTime, int cascadeNumber);
void stateUpdate();
void setFreezer();
void writeCommandsEerprom(uint8_t commandArray[], int commandLength );
void readCommandsEerprom(uint8_t *commandArray );
void receiveCommandsSerial(uint8_t *commandArray );



//variable intialization, set solenoids and order for auto mode here
volatile int operationMode = closeAllPumpOff;
int numberToExecute = 0;
unsigned long tempTime = 0;
unsigned long tempONTime = 0;
unsigned long tempOFFTime = 0;
unsigned long cascadeTimer = 0;
unsigned long pumpPulseTime = 0;
volatile bool pumpState = 0;
bool prevPumpValue = 0;
bool pumpProportion = 100;
int cascadeSolenoid =0;
int cascadeCycle = 0;
int tempCount = 0;
bool tempRising = 0;
double previousTemp = 0;
double currentTemp = 0;
volatile int introTime = -1;
bool light = 0;
uint64_t setValue = 0;
int hourCountdown = 0;

uint64_t prevSolValue = 0;
String incomingByte;
String strHour;
String strMinute;
String strSecond;
String receivedString;
String strTemp;
String strMode;

/* Programming
250 = do nothing this hour
20X = do next X many commands this cycle
200 = flush Chamber
15X = Go to X mode
1XX = set temp to XX
<100 = open solenoid
*/

uint8_t autoSolenoids[80];
double tempBuffer[tempBufferSize];
int autoCycle = 0;
int setTemp = 3;

int ho = 0;
int mi = 0;
int x = 0;

IntervalTimer myTimer;

void setup()
{
  EXPANDER_Init();
  SOL_Init();
  SWITCH_Init();
  MISC_Init();

  Serial.begin(115200);

  tempTime = 0;
  tempRising = 0;
  previousTemp = 0;
  currentTemp = 0;
  introTime = 0;
  operationMode = closeAllPumpOff;
  autoCycle = 0;
  setTemp = 3;
  numberToExecute = 0;


  pinMode(13, OUTPUT);
  pinMode(16, OUTPUT);
  myTimer.begin(pumpPulse, 500000);

//  readCommandsEerprom(autoSolenoids);

}

void loop()
{
/*
  if ( pumpState )
  {
    MISC_PumpOn();
  }else{
    MISC_PumpOff();
  }
*/

  if (millis() - tempTime > 1000) {
    stateUpdate();
    light = !light; //Flash light so i know it's running.
    digitalWrite(13, light);

    tempTime = millis();
    tempBuffer[tempCount] = MISC_ReadTemp();
    tempCount++;

    if (tempCount > tempBufferSize)
    {
      tempCount = 0;
      currentTemp = 0;
      previousTemp = currentTemp;
      for (uint8_t i = 0; i < tempBufferSize; i++)
      {
        currentTemp = currentTemp + tempBuffer[i];

      }
      currentTemp = currentTemp/tempBufferSize;

    }


    x = Serial.available();
    Serial.println(x);
    if(x){
      receivedString = Serial.readString();
      if (receivedString.startsWith("Time"))
      {
        strHour = receivedString.substring(5,7);
        strMinute = receivedString.substring(8,10);
        strSecond = receivedString.substring(11,13);
        setTime(strHour.toInt(),strMinute.toInt(), strSecond.toInt(), 0,0, 0);
        introTime = hour(now());
      }else if(receivedString.startsWith("reprogram"))
      {
        receiveCommandsSerial(autoSolenoids);
//        writeCommandsEerprom(autoSolenoids, sizeof(autoSolenoids)/sizeof(uint8_t));
      }
      else if(receivedString.startsWith("reset"))
      {
        autoCycle = 0;
      }else if(receivedString.startsWith("temp")){
        strTemp = receivedString.substring(5,7);
        Serial.print("Temp set to: ");
        Serial.println(strTemp);
        setTemp = strTemp.toInt();
      }else if(receivedString.startsWith("mode")){
        strMode = receivedString.substring(5,7);
        Serial.print("Mode set to: ");
        Serial.println(strMode);
        operationMode = strMode.toInt();
      }else{
        // say what you got:
        Serial.print("I received: ");
        Serial.println(receivedString);
      }
    }


    Serial.println("reset");
    Serial.print("THis Wurlitzer shall be known as ");
    Serial.println(wurlitzerName);
    Serial.print("Current temp = ");
    Serial.println(currentTemp);
    Serial.print("Target temp = ");
    Serial.println(setTemp);
    Serial.print("current mode = ");
    Serial.println(operationMode);
    Serial.print("next command = ");
    Serial.print(autoSolenoids[autoCycle]);
    Serial.print(" will be in ");
    Serial.print((60 - minute(now())));
    Serial.println("minutes");
    setFreezer();

  }

  prevPumpValue = pumpState;
  prevSolValue = setValue;
  setValue = 0;

  switch (operationMode) {
    case flushPrimeChamber:
    {
      setValue |= (1LL << 27);
      setValue |= (1LL << 31);
      setValue |= (1LL << 32);
      setValue |= (1LL << 33);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case flushSystem:
    {
      setValue |= (1LL << 26);
      setValue |= (1LL << 27);
      setValue |= (1LL << 30);
      setValue |= (1LL << 33);
      setValue |= cascade(setValue,2000, 5);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case flushTest:
    {
      setValue |= (1LL << 26);
      setValue |= (1LL << 29);
      setValue |= (1LL << 30);
      setValue |= (1LL << 32);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case cascadeMode:
    {
      SOL_SetAll(setValue);
      setValue |= cascade(setValue,1000, 9);
    }
    break;
    case flushDoses:
    {
      setValue |= (1LL << 29);
      setValue |= (1LL << 31);
      setValue = cascade(setValue,2000, 5);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case flushPlumbing:
    {
      setValue |= (1LL << 29);
      setValue |= (1LL << 31);
      setValue |= (1LL << 32);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case PrimeDoses:
    {
      setValue |= (1LL << 27);
      setValue |= (1LL << 28);
      setValue = cascade(setValue,2000, 5);
      SOL_SetAll(setValue);
      pumpState = 1;
    }
    break;
    case autoDose:
    {

      if (((((hour(now()) - introTime >= 1) || ((introTime == 23) && (hour(now())== 0))) && minute(now()) == 53)  || (numberToExecute > 0)) ) {

        numberToExecute--;
        introTime = hour(now());
        if(numberToExecute <= 0)
        {
          numberToExecute = 0;
        }else
        {
          setValue = prevSolValue;
        }

        Serial.println("introduction time");
        Serial.println("reqTime");

        if((autoSolenoids[autoCycle] >= 100) && (autoSolenoids[autoCycle] < 150))
        {
          setTemp = (autoSolenoids[autoCycle] - 100);
          Serial.print(autoSolenoids[autoCycle]);
          Serial.print(": event: Temp = ");
          Serial.println(setTemp);
          setValue |= (1LL << 25);
          setValue |= (1LL << 26);
          setValue |= (1LL << 32);
          SOL_SetAll(setValue);
          pumpState = 1;
        }

        else if ((autoSolenoids[autoCycle] > 200) && (autoSolenoids[autoCycle] < 250))
        {
          numberToExecute =  (autoSolenoids[autoCycle] - 200);
          Serial.print(autoSolenoids[autoCycle]);
          Serial.print(": event:  do ");
          Serial.print(numberToExecute);
          Serial.println(" commands");
        }
        else if ((autoSolenoids[autoCycle] >= 150) && (autoSolenoids[autoCycle] < 200))
        {
          Serial.print(autoSolenoids[autoCycle]);
          Serial.print(": event: go to mode ");
          operationMode =  (autoSolenoids[autoCycle] - 150);
          Serial.print(autoSolenoids[autoCycle] - 150);
        }

        else if (autoSolenoids[autoCycle] == 200)
        {
          pumpState = 0;
          setValue |= (1LL << 26);
          setValue |= (1LL << 29);
          setValue |= (1LL << 30);
          setValue |= (1LL << 32);
          SOL_SetAll(setValue);
          pumpState = 1;
          Serial.print(autoSolenoids[autoCycle]);
          Serial.println(": event: Flushing Chamber");
        }

        else if (autoSolenoids[autoCycle] == 250)
        {
          Serial.print(autoSolenoids[autoCycle]);
          Serial.println(": event: do nothing");
          setValue |= (1LL << 25);
          setValue |= (1LL << 26);
          setValue |= (1LL << 32);
          SOL_SetAll(setValue);
          pumpState = 1;
        }else if (autoSolenoids[autoCycle] == 255)
        {
          operationMode = flushSystem;
        }else if ((autoSolenoids[autoCycle] >= 50) && (autoSolenoids[autoCycle] < 100))
        {
          if (hourCountdown >1)
          {
            Serial.print("event: Waiting ");
            Serial.print(hourCountdown);
            Serial.println(" more hours.");
            autoCycle--;
          }else if (hourCountdown == 1)
          {
            Serial.print("event: Waiting ");
            Serial.print(hourCountdown);
            Serial.println(" more hours.");
          }else if(hourCountdown == 0)
          {
            hourCountdown = (autoSolenoids[autoCycle] - 50);
            autoCycle--;
            Serial.print("event: Setting Timer for");
            Serial.print(hourCountdown);
            Serial.println(" hours.");
          }
          hourCountdown--;
        }
        else
        {
          pumpState = 0;
          setValue |= (1LL << 25);
          setValue |= (1LL << 26);
          setValue |= autoMode(autoCycle);
          SOL_SetAll(setValue);
          pumpState = 1;
        }
        autoCycle++;

        if (numberToExecute < 0)
        numberToExecute = 0;
      }
    }
    break;
    case closeAllPumpOff:
    {
      SOL_SetAll(setValue);
      pumpState = 0;
    }
    break;
    case manual:
    {
      Serial.println("manual");
      //needs work for staying in and booting out of this mode
      // uint64_t t = 0;
      // uint16_t switches = SWITCH_ReadAll();
      // for (uint8_t i = 0; i < 12; i++) {
      //   if (switches & (1 << i))
      //   {
      //     t |= (1LL << (i));
      //     Serial.println(i);
      //   }
      // }
    }
    break;
    default:
    {
      //Serial.println("modefault");
    }
    break;


  }




}

void setFreezer()
{
  if (previousTemp < currentTemp) {
    tempRising = 1;
  } else {
    tempRising = 0;
  }
  if ((currentTemp - setTemp) > 1) {
    MISC_TempSSROn();
  } else if (millis() - tempONTime < 20000){
    MISC_TempSSROn();
  }else if (millis() - tempONTime < 150000){
    MISC_TempSSROff();
  }
  else if (tempRising && (currentTemp > setTemp)) {
    tempONTime = millis();
    Serial.println("it's hot G");
  }else{
    MISC_TempSSROff();
  }
}

void stateUpdate()
{
  uint16_t switches = SWITCH_ReadAll();
  for (uint8_t i = 0; i < 12; i++) {
    if (switches & (1 << i))
    {
      operationMode = i;
      introTime = hour(now());
      cascadeTimer = millis();
      cascadeCycle = 0;
      cascadeSolenoid = 0;
      setValue =0;
      SOL_SetAll(0);
      Serial.print(autoSolenoids[autoCycle]);
      Serial.print("mode changed to");
      Serial.println(i);
      pumpState = 0;
    }
  }
}


uint64_t autoMode (uint8_t cycle)
{
  uint64_t setValue =0;
  if((sizeof(autoSolenoids)/sizeof(uint8_t)) > cycle)
  {
    setValue |= (1LL << autoSolenoids[cycle]);
    Serial.print(autoSolenoids[autoCycle]);
    Serial.print(": event: Solenoid opened = ");
    Serial.println(autoSolenoids[cycle]);
  }else{
    operationMode = flushSystem;
  }
  return setValue;
}

uint64_t cascade(uint64_t soldenoidsActive, uint16_t pulseTime, int cascadeNumber)
{

  volatile uint64_t returnSolenoids = soldenoidsActive;

  returnSolenoids |= (1LL << (cascadeSolenoid));
  if((millis() - cascadeTimer > pulseTime))
  {
    cascadeSolenoid ++;
    if (cascadeSolenoid > 24)
    {
      cascadeCycle++;
      cascadeSolenoid = 0;
    }
    if (cascadeCycle > cascadeNumber)
    {
      cascadeSolenoid =0;
      cascadeCycle = 0;
      operationMode = flushPlumbing;
      returnSolenoids = 0;
    }
    cascadeTimer = millis();
  }
  return returnSolenoids;
}

void writeCommandsEerprom(uint8_t commandArray[],int commandLength )
{
  int i = 0;
  for (i = 0; i == commandLength; i++)
  {
    EEPROM.write(i, commandArray[i]);
  }
}

void readCommandsEerprom(uint8_t *commandArray )
{
  int i = 0;

  while (EEPROM.read(i) != 255)
  {
    commandArray[i] = EEPROM.read(i);
    i++;
  }
}


void receiveCommandsSerial(uint8_t* commandArray )
{
  uint8_t receivedByte;
  int i = 0;

  x = Serial.available();
  while(x){
    receivedByte = Serial.read();
  }
  Serial.println("ready");

  while (Serial.available() ==0){
    delay(10);
  }

  while (Serial.available() > 0) {

    receivedByte = Serial.parseInt(); // read the incoming byte:
    commandArray[i] = receivedByte;
    i++;
  }
  commandArray[i] = 255;
  autoCycle = 0;
  Serial.println("event: Command Upload Complete");
}


void pumpPulse(void) {
  if (pumpState)
  {
    digitalWrite(16,HIGH);
  }else{
    digitalWrite(16, LOW);
  }
}
